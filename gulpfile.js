var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var pump = require('pump');

gulp.task('default', defaultTask);

function defaultTask(done) {
  console.log("Rode o comando gulp scss ou gulp watch para automatizar um dos processos");
  done();
}

/*
task para automatizar a criação do CSS à partir do Scss, copiando cada arquivo de
sua respectiva pasta dentro do diretório SCSS para sua respectiva pasta dentro do diretório do CSS
*/
gulp.task('automate-processes',
    function(){
        scssFunction();
        scssFunctionCompress();
        scssVendorFunctionCompress();
        jsFuntionCompress();
    }
 );

function jsFuntionCompress(){
    return gulp.src(['./projeto_combate_app/static/js/*.js', '!./projeto_combate_app/static/js/*min.js'])
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./projeto_combate_app/static/js/'))
}

function scssFunction() {
 return gulp.src('./projeto_combate_app/static/scss/*.scss')
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(gulp.dest('./projeto_combate_app/static/css'));
}

function scssFunctionCompress() {
 return gulp.src('./projeto_combate_app/static/scss/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./projeto_combate_app/static/css'));
}

function scssVendorFunctionCompress() {
 return gulp.src('./projeto_combate_app/static/scss/**/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./projeto_combate_app/static/css'));
}

/*
task para o watch do Scss, fazendo com que o gulp fique em constante atualização
de acordo com as modificações feitas nos arquivos SCSS. Toda vez que algo é alterado
no SCSS o Gulp detecta e gera novamente o arquivo CSS.
*/
gulp.task('watch', watchScss);

function watchScss() {
    gulp.watch('./projeto_combate_app/static/js/*.js', ['automate-processes']);
    gulp.watch('./projeto_combate_app/static/scss/*.scss', ['automate-processes']);
    gulp.watch('./projeto_combate_app/static/scss/**/*.scss', ['automate-processes']);
    gulp.watch('./projeto_combate_app/static/scss/**/**/**/*.scss', ['automate-processes']);
}