from django.db import models
from django.utils import timezone
from datetime import date

class EventDefault(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, verbose_name = 'Autor')
    title = models.CharField(max_length=100, verbose_name = 'Título Interno da Box de Eventos (Para exibição no admin)')
    event_title = models.CharField(max_length=200, verbose_name = 'Título principal da Box de Eventos')
    events_button_text = models.CharField(max_length=20, verbose_name = 'Cor do Texto do Botão')
    events_button_color = models.CharField(max_length=7, verbose_name = 'Cor do Botão')
    created_date = models.DateTimeField(auto_now_add=True, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        verbose_name_plural = "Campos Básicos da Caixa de Evento"

    def publish(self):
        self.modified_date = timezone.now()
        self.save()

    def __str__(self):
        return self.event_title

class Event(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, verbose_name = 'Autor')
    date = models.DateTimeField(default=timezone.now, verbose_name = 'Dia e Horário do Evento')
    image = models.ImageField(verbose_name = 'Capa da Luta')
    type_fight = models.CharField(max_length=200, verbose_name = 'Tipo da Luta')
    fighter1 = models.CharField(max_length=200, verbose_name = 'Lutador 1')
    fighter2 = models.CharField(max_length=200, verbose_name = 'Lutador 2')
    created_date = models.DateTimeField(auto_now_add=True, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        verbose_name_plural = "Eventos"

    def publish(self):
        self.modified_date = timezone.now()
        self.save()

    def __str__(self):
        return str(self.date)



