from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class NavigationFooter(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, verbose_name = 'Autor')
    name = models.CharField(max_length=200, verbose_name = 'Nome')
    link = models.CharField(max_length=200, verbose_name = 'Link de Destino')
    image = models.ImageField(blank=True, null=True, verbose_name = 'Imagem')
    order = models.PositiveIntegerField(default=0, validators=[MinValueValidator(1), MaxValueValidator(100)], verbose_name = 'Posição')

    class Meta:
        verbose_name_plural = "Menu do Rodape"

    def publish(self):
        self.save()

    def __str__(self):
        return str(self.order) + " - " + self.name



