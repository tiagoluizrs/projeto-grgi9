from django.db import models
from django.utils import timezone

class DevicesArea(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, verbose_name = 'Autor')
    title = models.CharField(max_length=100, verbose_name = 'Título Interno da chamada (Para exibição no admin)')
    image = models.ImageField(verbose_name = 'Imagem da Chamada do Aplicativo')
    text = models.TextField(verbose_name = 'Texto da Chamada do Aplicativo')
    button_link = models.CharField(max_length=200, verbose_name = 'Link do Botão')
    created_date = models.DateTimeField(auto_now_add=True, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        verbose_name_plural = "Caixa de Anúncio do Aplicativo"

    def publish(self):
        self.modified_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title