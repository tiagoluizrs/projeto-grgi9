from django.db import models
from tinymce.models import HTMLField
from django.utils import timezone
from colorfield.fields import ColorField

class DefaultConfig(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, verbose_name = 'Título do Slide')
    title = models.CharField(max_length=100, verbose_name = 'Título dessa modificação')
    top_bar = ColorField(default='#D32127', verbose_name = 'Cor da Barra superior do site')
    logo = models.ImageField(verbose_name = 'Logo do Site (Fica na barra superior do site)')
    welcome_title = models.CharField(max_length=200, verbose_name = 'Título de Boas Vindas (Abaixo do Slide Principal)')
    welcome_text = HTMLField(verbose_name = 'Texto de Boas Vindas (Abaixo do Slide Principal)')
    copyright_text = models.CharField(max_length=400, verbose_name = 'Texto do Copyright')
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Configurações Básicas do Site"

    def publish(self):
        self.modified_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title + " - Última modificação em " + self.modified_date.strftime('%m/%d/%Y')