from django.db import models
from tinymce.models import HTMLField

class Operator(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, verbose_name='Autor')
    name = models.CharField(max_length=200, verbose_name='Nome da Operadora')
    logo = models.ImageField(verbose_name='Logo da Operadora')
    tel1 = models.CharField(max_length=200, blank=True, null=True, verbose_name='Telefone 1 da Operadora para Assinatura')
    text_tel1 = models.CharField(max_length=200, blank=True, null=True, verbose_name='Texto do Telefone 1')
    tel2 = models.CharField(max_length=200, blank=True, null=True, verbose_name='Telefone 2 da Operadora para Assinatura')
    text_tel2 = models.CharField(max_length=200, blank=True, null=True, verbose_name='Texto do Telefone 2')
    name = models.CharField(max_length=200, verbose_name='Nome da Operadora')
    site = models.CharField(max_length=200, blank=True, null=True, verbose_name='Site da Operadora')
    extra_buy_mode = HTMLField(blank=True, null=True, verbose_name='Método Extra de Adquirir Plano')

    class Meta:
        verbose_name_plural = "Operadoras"

    def publish(self):
        self.save()

    def __str__(self):
        return self.name




