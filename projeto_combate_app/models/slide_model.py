from django.db import models
from django.utils import timezone
from colorfield.fields import ColorField

class Slide(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, verbose_name = 'Autor')
    title = models.CharField(max_length=200, verbose_name = 'Título do Slide')
    subtitle = models.CharField(max_length=150, verbose_name = 'Subtítulo do Slide')
    image = models.ImageField(verbose_name = 'Imagem')
    text = models.TextField(verbose_name = 'Descrição do Slide')
    button_text = models.CharField(max_length=20, verbose_name = 'Texto do Botão')
    button_color = ColorField(default='#D32127', verbose_name = 'Cor do Botão')
    created_date = models.DateTimeField(auto_now_add=True, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        verbose_name_plural = "Slide Principal do Site"

    def publish(self):
        self.modified_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title