from django.shortcuts import render
from django.utils import timezone
from .models.default_config_model import DefaultConfig
from .models.devices_area import DevicesArea
from .models.event_model import Event, EventDefault
from .models.navigation_footer_model import NavigationFooter
from .models.operator_model import Operator
from .models.slide_model import Slide


def home(request):
    defaultConfigs = DefaultConfig.objects.filter(modified_date__lte=timezone.now()).order_by('-modified_date')
    devicesArea = DevicesArea.objects.filter(modified_date__lte=timezone.now()).order_by('-modified_date')
    event_default = EventDefault.objects.filter(modified_date__lte=timezone.now()).order_by('-modified_date')
    events = Event.objects.filter(modified_date__lte=timezone.now()).order_by('-modified_date')
    navigationFooter = NavigationFooter.objects.filter().order_by('order')
    operators = Operator.objects.filter().order_by('name')
    slides = Slide.objects.filter(modified_date__lte=timezone.now()).order_by('modified_date')

    return render(request, 'hotsite/home.html', {
        'defaultConfigs': defaultConfigs,
        'devicesArea': devicesArea,
        'event_default': event_default,
        'events': events,
        'navigationFooter': navigationFooter,
        'operators': operators,
        'slides': slides
    })
