from django.apps import AppConfig


class ProjetoCombateAppConfig(AppConfig):
    name = 'projeto_combate_app'
    verbose_name = 'Projeto Combate GRGi9'
