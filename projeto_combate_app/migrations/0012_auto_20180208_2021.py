# Generated by Django 2.0.2 on 2018-02-08 22:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projeto_combate_app', '0011_defaultconfigs_copyright_text'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='eventdefault',
            name='button_color',
        ),
        migrations.RemoveField(
            model_name='eventdefault',
            name='button_text',
        ),
        migrations.AlterField(
            model_name='defaultconfigs',
            name='copyright_text',
            field=models.CharField(max_length=400),
        ),
        migrations.AlterField(
            model_name='defaultconfigs',
            name='title',
            field=models.CharField(max_length=20),
        ),
    ]
