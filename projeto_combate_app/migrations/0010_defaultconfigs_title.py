# Generated by Django 2.0.2 on 2018-02-08 22:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projeto_combate_app', '0009_auto_20180208_2008'),
    ]

    operations = [
        migrations.AddField(
            model_name='defaultconfigs',
            name='title',
            field=models.CharField(default='', max_length=20),
        ),
    ]
