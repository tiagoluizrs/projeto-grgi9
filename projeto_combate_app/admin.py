from django.contrib import admin
from .models.default_config_model import DefaultConfig
from .models.devices_area import DevicesArea
from .models.event_model import Event, EventDefault
from .models.navigation_footer_model import NavigationFooter
from .models.operator_model import Operator
from .models.slide_model import Slide

admin.site.register(DefaultConfig)
admin.site.register(DevicesArea)
admin.site.register(Operator)
admin.site.register(EventDefault)
admin.site.register(Event)
admin.site.register(NavigationFooter)
admin.site.register(Slide)