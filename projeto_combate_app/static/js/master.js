var staticRoot = $("#staticRoot").val() + 'uploads/';

$(document).ready(function(){
    loadCarousel();
    $('#preloader').remove();
});

$('.l-grid-image').click(function(){
    $('#all_operators').hide();
    $('#operator_specific').show();

    var logo = $($(this).find(".operator_logo")).val();
    var site = $($(this).find(".operator_site")).val();
    var tel1 = $($(this).find(".operator_tel1")).val();
    var text_tel1 = $($(this).find(".operator_text_tel1")).val();
    var tel2 = $($(this).find(".operator_tel2")).val();
    var text_tel2 = $($(this).find(".operator_text_tel2")).val();
    var extra_buy = $($(this).find(".operator_extra_buy")).val();

    if(site == '' || site == 'None' || site == null){
        $('#body-site').hide();
    }else{
        $('#body-site').show();
    }

    if((tel1 == '' || tel1 == 'None' || tel1 == null) && (tel2 == '' || tel2 == 'None' || tel2 == null)){
        $('#body-tels').hide();
    }else{
        $('#body-tels').show();

        if(tel1 == '' || tel1 == 'None' || tel1 == null){
            $('#modal_tel1').hide();
            $('#modal_text_tel1').hide();
        }else{
            $('#modal_tel1').show();
            $('#modal_text_tel1').show();
        }

        if(text_tel1 == '' || text_tel1 == 'None' || text_tel1 == null){
            $('#modal_text_tel1').hide();
        }else{
            $('#modal_text_tel1').show();
        }

        if(tel2 == '' || tel2 == 'None' || tel2 == null){
            $('#modal_tel2').hide();
            $('#modal_text_tel2').hide();
        }else{
            $('#modal_tel2').show();
            $('#modal_text_tel2').show();
        }

        if(text_tel2 == '' || text_tel2 == 'None' || text_tel2 == null){
            $('#modal_text_tel2').hide();
        }else{
            $('#modal_text_tel2').show();
        }

        if((tel1 == '' || tel1 == 'None' || tel1 == null) || (tel2 == '' || tel2 == 'None' || tel2 == null)){
            $('#or').hide();
        }else{
            $('#or').show();
        }
    }

    if(extra_buy == '' || extra_buy == 'None' || extra_buy == null){
        $('#body-extra').hide();
    }else{
        $('#body-extra').show();
    }

    $("#modal_logo").css('background-image', 'url(' + staticRoot + logo + ')');
    $("#modal_site").attr('href', site);
    $("#modal_tel1").text(tel1);
    $("#modal_text_tel1").text("(" + text_tel1 + ")");
    $("#modal_tel2").text(tel2);
    $("#modal_text_tel2").text("(" + text_tel2 + ")");
    $("#modal_extra_buy").html(extra_buy);
});

$('.btn-sign, .btn-return').click(function(){
    $('#operator_specific').hide();
    $('#all_operators').show();
});

function loadCarousel(){
    $('#rcarousel').slick({
      centerMode: true,
      centerPadding: '60px',
      slidesToShow: 5,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 2000,
      lazyLoad: 'ondemand',
      responsive: [
        {
          breakpoint: 992,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 3
          }
        },
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
          }
        }
      ]
    });
}