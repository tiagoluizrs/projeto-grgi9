# Projeto GRGi9

## Descrição do Projeto
Projeto desenvolvido como teste de conhecimento na área de Desenvolvimento Full-Stack. O projeto foi desenvolvido com as seguintes linguagens e tecnologias:

 - **Back-end:**
	 - Python (Linguagem para o Back-end do sistema):
		 - Django (Framework Python utilizado no Back-end).

 - **Front-end:**
	 - JavaScript (Linguagem para o Front-End do sistema
	 - HTML5 (Linguagem para o Front-End do sistema););
	 - CSS3 (Com os padrões BEM e SMASS);
	 - Bootstrap (Framework Visual);
	 - Slick (Biblioteca JavaScript para criação de galerias);
	 - SASS (SCSS);
	 - jQuery (Biblioteca JavaScript);
	 - Gulp (Automatizador de Processos).
	 
## Pré-requisitos para execução do projeto

Para que o projeto rode em sua máquina corretamente você precisará ter os seguintes itens instalados:

 - Python v3.6.3 ou superior;
 - Node v6.11.1 ou superior;
 - Gulp 3.9.1 (No projeto) 2.0.1 (Global);
 - Banco de Dados MySQL.

> **Nota:** O **Gulp.js** não é necessário para rodar a aplicação, apenas para automatizar os processos de desenvolvimento do projeto.

## Configurando o ambiente

Após realizar o clone do projeto em sua máquina, siga os passos que estão abaixo:

### Instalando o Python 3.6
#### Window
Primeiro verifique se o seu computador está rodando uma versão 32-bit ou uma versão 64-bit.
Vá até a url https://www.python.org/downloads/windows/ e procure a versão mais atualizada do Python, ou a mesma indicada no Readme.
Após baixar o Executável clique nele para executá-lo.
> **Nota:** Uma coisa para se atentar: Durante a instalação você irá notar uma janela com a marcação **"Setup"**. Certifique-se de selecionar **"Add Python 3.x to PATH"** na caixa de seleção e clicar em **"Install Now"**, como mostrado aqui: 


#### Linux
Digite o comando conforme abaixo:
```
	sudo apt-get install python3.6 
```
#### OS X
> **Nota:** Antes de você instalar o **Python** no **OS X**, você deve ter certeza que as configurações de seu Mac permitem a instalação de pacotes que não são da **App Store**. Vá até **Preferências do Sistema** (está na pasta Aplicações), clique em **"Segurança & Privacidade,"** e então na aba **"General"**. Se estiver configurado **"Mac App Store"** em **"Allow apps downloaded from:"**, altere para **"Mac App Store and identified developers."**

Agora vá no site https://www.python.org/downloads/release/python-351/ e baixe o instalador do Python:
 - Baixe o arquivo Mac OS X 64-bit/32-bit installer;
 - Duplo clique em python-3.5.1-macosx10.6.pkg para rodar o instalador.    

## Instalando o PIP 
Faça o download do [get-pip.py](https://bootstrap.pypa.io/get-pip.py) navegue até o diretório em que o arquivo foi baixado e digite o seguinte comando
#### Windows
```
C:\Users\Tiago\Documents\projeto-grgi9> python get-pip.py
```
#### Linux and OS X
```
$ python get-pip.py
```

### Criando seu ambiente virtual (VirtualEnv)
Vá com o terminal até a pasta em que seu projeto se encontra e digite o seguinte comando para criar um Ambiente Virtual (VirtualEnv):

#### Windows
```
C:\Users\Tiago\Documents\projeto-grgi9> C:\Python36\python -m venv virtualenv_projeto_combate
```
#### Linux and OS X
```
$ python3 -m venv virtualenv_projeto_combate
```
### Iniciando o Ambiente Virtual (VirtualEnv)
#### Windows
```
C:\Users\Tiago\Documents\projeto-grgi9> virtualenv_projeto_combate\Scripts\activate
```
#### Linux and OS X
```
$ source virtualenv_projeto_combate/bin/activate
```

## Instalando os módulos do Python
Atualize seu pip (Caso tenha acabado de instalar não será preciso rodar esse comando).
#### Windows, Linux, Os x
```
(virtualenv_projeto_combate) ~$ pip install --upgrade pip
```

Agora rode o seguinte comando
#### Windows, Linux, Os x
```
(virtualenv_projeto_combate) ~$ pip install requirements.txt
```
> **Nota:** Se por acaso o comando não funcionar rode-o da seguinte maneira **pip install --upgrade -r requirements.txt** 

## Instalando o Node
#### Windows
Primeiro verifique se o seu computador está rodando uma versão 32-bit ou uma versão 64-bit e depois vá até o site do [Nodejs](https://nodejs.org/en/download/) e faça o download para sua máquina. Após baixar faça a instalação.

#### Linux
```
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```
#### OS x
Faça o download do instalador no site do [Nodejs](https://nodejs.org/en/download/) e depois rode o comando abaixo.
> **Nota:** Seu arquivo precisará estar na pastas de **downloads**  
```
curl "https://nodejs.org/dist/latest/node-${VERSION:-$(wget -qO- https://nodejs.org/dist/latest/ | sed -nE 's|.*>node-(.*)\.pkg</a>.*|\1|p')}.pkg" > "$HOME/Downloads/node-latest.pkg" && sudo installer -store -pkg "$HOME/Downloads/node-latest.pkg" -target "/"
```
## Instalando o Gulp
Rode os comandos abaixo para instalar o gulp em seu
#### Windows, Linux, OS x 
```
$ npm install -g gulp
```
### Baixando as dependências do Gulp
Rode o comando abaixo para instalar as dependências do Gulp.
#### Windows, Linux, Os X
```
$ npm install
```

## Instalando o MyQSL```
#### Windows
Primeiro verifique se o seu computador está rodando uma versão 32-bit ou uma versão 64-bit e depois vá até o site do [MySQL](https://dev.mysql.com/downloads/mysql/) e faça o download da versão correta para seu sistema operacional.

#### Linux, Os X
```
$ sudo apt-get update
$ sudo apt-get install mysql-server
$ mysql_secure_installation
```

Após fazer isso crie um banco de dados com o nome do banco que existe no projeto **"projeto_combate_grgi9"**.


## Configurando o Banco de Dados
Para alterar os dados de conexão do banco de dados vá até o arquivo **settings.py** que fica dentro da pasta **"projeto_combate"** navegue até  a na linha 81  aproximadamente e alterar as chaves da constante **DATABASES** 

## Fazendo o Migrate do Banco de Dados
Rode os seguintes comando abaixo para criar a migração e executá-la em seu banco de dados.

#### Windows, Linux, Os X
```
(virtualenv_projeto_combate) $ python manage.py makemigrations
```
```
(virtualenv_projeto_combate) $ python manage.py migrate
```
## Criando um super admin
Para criar o administrador do sistema execute o comando abaixo
> **Nota:** Se preferir pode utilizar o já criado mas para isso terá que ter feito backup do banco que está na pasta raiz do projeto. 
> Usuário: admin
> Sehha: 1234qwer
#### Windows, Linux, Os X
```
(virtualenv_projeto_combate) $python manage.py createsuperuser
```
## Rodando seu Projeto
Com o terminal aberto rode o seguinte comando para executar o servidor
```
python manage.py runserver
```

> **Nota:** Se por algum inconveniente o projeto não executar o migrate corretamente, o processo poderá ser feito de forma manual, indo até a pasta raiz e pegando o arquivo sql que está salvo lá.