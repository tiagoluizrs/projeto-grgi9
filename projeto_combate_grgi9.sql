-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 09-Fev-2018 às 08:50
-- Versão do servidor: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projeto_combate_grgi9`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add permission', 1, 'add_permission'),
(2, 'Can change permission', 1, 'change_permission'),
(3, 'Can delete permission', 1, 'delete_permission'),
(4, 'Can add group', 2, 'add_group'),
(5, 'Can change group', 2, 'change_group'),
(6, 'Can delete group', 2, 'delete_group'),
(7, 'Can add user', 3, 'add_user'),
(8, 'Can change user', 3, 'change_user'),
(9, 'Can delete user', 3, 'delete_user'),
(10, 'Can add content type', 4, 'add_contenttype'),
(11, 'Can change content type', 4, 'change_contenttype'),
(12, 'Can delete content type', 4, 'delete_contenttype'),
(13, 'Can add log entry', 5, 'add_logentry'),
(14, 'Can change log entry', 5, 'change_logentry'),
(15, 'Can delete log entry', 5, 'delete_logentry'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add slide', 7, 'add_slide'),
(20, 'Can change slide', 7, 'change_slide'),
(21, 'Can delete slide', 7, 'delete_slide'),
(22, 'Can add default configs', 8, 'add_defaultconfigs'),
(23, 'Can change default configs', 8, 'change_defaultconfigs'),
(24, 'Can delete default configs', 8, 'delete_defaultconfigs'),
(25, 'Can add devices area', 9, 'add_devicesarea'),
(26, 'Can change devices area', 9, 'change_devicesarea'),
(27, 'Can delete devices area', 9, 'delete_devicesarea'),
(28, 'Can add event', 10, 'add_event'),
(29, 'Can change event', 10, 'change_event'),
(30, 'Can delete event', 10, 'delete_event'),
(31, 'Can add event default', 11, 'add_eventdefault'),
(32, 'Can change event default', 11, 'change_eventdefault'),
(33, 'Can delete event default', 11, 'delete_eventdefault'),
(34, 'Can add operator', 12, 'add_operator'),
(35, 'Can change operator', 12, 'change_operator'),
(36, 'Can delete operator', 12, 'delete_operator'),
(37, 'Can add navigation footer', 13, 'add_navigationfooter'),
(38, 'Can change navigation footer', 13, 'change_navigationfooter'),
(39, 'Can delete navigation footer', 13, 'delete_navigationfooter'),
(40, 'Can add default config', 8, 'add_defaultconfig'),
(41, 'Can change default config', 8, 'change_defaultconfig'),
(42, 'Can delete default config', 8, 'delete_defaultconfig');

-- --------------------------------------------------------

--
-- Estrutura da tabela `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$100000$DzI4Fkb0xghU$HI35IGAiNY+ClAmBNcis6BQ9ba7yGzIMcNBDNNM0Gls=', '2018-02-08 17:23:27.703919', 1, 'admin', '', '', 'admin@teste.com', 1, 1, '2018-02-08 17:22:50.137749');

-- --------------------------------------------------------

--
-- Estrutura da tabela `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2018-02-08 17:25:24.640741', '1', 'ASSINE O COMBATE: 1', 1, '[{\"added\": {}}]', 7, 1),
(2, '2018-02-08 17:25:42.131458', '2', 'ASSINE O COMBATE: 2', 1, '[{\"added\": {}}]', 7, 1),
(3, '2018-02-08 18:15:00.065098', '2', 'ASSINE O COMBATE: 2', 2, '[{\"changed\": {\"fields\": [\"button_color\"]}}]', 7, 1),
(4, '2018-02-08 18:21:07.910735', '2', 'ASSINE O COMBATE: 2', 2, '[{\"changed\": {\"fields\": [\"text\"]}}]', 7, 1),
(5, '2018-02-08 21:46:57.758855', '3', 'A MAIOR COBERTURA DO UFC', 1, '[{\"added\": {}}]', 8, 1),
(6, '2018-02-08 21:49:46.634595', '3', 'A MAIOR COBERTURA DO UFC', 2, '[]', 8, 1),
(7, '2018-02-08 21:54:06.311502', '4', 'Configuração Padrão de 2018-02-08 19:53:43-02:00', 1, '[{\"added\": {}}]', 8, 1),
(8, '2018-02-08 22:03:12.063023', '4', 'Configuração Padrão de 2018-02-08 19:53:43-02:00', 2, '[{\"changed\": {\"fields\": [\"top_bar\"]}}]', 8, 1),
(9, '2018-02-08 22:03:19.884758', '3', 'Configuração Padrão de 2018-02-08 19:49:31-02:00', 2, '[]', 8, 1),
(10, '2018-02-08 22:06:20.052089', '3', 'Configuração Padrão de 2018-02-08 19:49:31-02:00', 2, '[]', 8, 1),
(11, '2018-02-08 22:09:05.573306', '4', 'Configuração Padrão de None', 2, '[]', 8, 1),
(12, '2018-02-08 22:09:38.176439', '4', 'Configuração Padrão de 2018-02-08 20:09:37-02:00', 2, '[{\"changed\": {\"fields\": [\"modified_date\"]}}]', 8, 1),
(13, '2018-02-08 22:09:46.625032', '3', 'Configuração Padrão de 2018-02-08 20:09:45-02:00', 2, '[{\"changed\": {\"fields\": [\"modified_date\"]}}]', 8, 1),
(14, '2018-02-08 22:10:20.618920', '3', 'Configuração Padrão de 2018-02-08 20:10:14-02:00', 2, '[{\"changed\": {\"fields\": [\"modified_date\"]}}]', 8, 1),
(15, '2018-02-08 22:10:33.489292', '4', 'Configuração Padrão de 2018-02-08 20:10:32-02:00', 2, '[{\"changed\": {\"fields\": [\"modified_date\"]}}]', 8, 1),
(16, '2018-02-08 22:10:41.474517', '4', 'Configuração Padrão de 2018-02-08 20:10:40-02:00', 2, '[{\"changed\": {\"fields\": [\"modified_date\"]}}]', 8, 1),
(17, '2018-02-08 22:13:27.208535', '3', 'Configuração Padrão de Configuração Padrão', 2, '[{\"changed\": {\"fields\": [\"title\"]}}]', 8, 1),
(18, '2018-02-08 22:13:44.861506', '4', '', 3, '', 8, 1),
(19, '2018-02-08 22:17:33.440806', '3', 'Configuração Padrão', 2, '[{\"changed\": {\"fields\": [\"copyright_text\"]}}]', 8, 1),
(20, '2018-02-08 22:22:59.309740', '2', 'PRÓXIMOS EVENTOS EXCLUSIVOS', 1, '[{\"added\": {}}]', 11, 1),
(21, '2018-02-08 22:23:04.599634', '2', 'PRÓXIMOS EVENTOS EXCLUSIVOS', 2, '[]', 11, 1),
(22, '2018-02-08 22:26:23.966059', '2', 'PRÓXIMOS EVENTOS EXCLUSIVOS', 2, '[{\"changed\": {\"fields\": [\"title\", \"modified_date\"]}}]', 11, 1),
(23, '2018-02-08 22:28:46.872243', '3', '2018-02-21 20:00:00-03:00', 1, '[{\"added\": {}}]', 10, 1),
(24, '2018-02-08 22:41:14.667332', '3', '2018-02-21 23:00:00+00:00', 3, '', 10, 1),
(25, '2018-02-08 22:41:41.973849', '4', '2018-02-21 20:41:31-03:00', 1, '[{\"added\": {}}]', 10, 1),
(26, '2018-02-08 22:42:35.110826', '5', '2018-02-08 20:42:19-02:00', 1, '[{\"added\": {}}]', 10, 1),
(27, '2018-02-08 22:42:42.981963', '6', '2018-02-08 20:42:37-02:00', 1, '[{\"added\": {}}]', 10, 1),
(28, '2018-02-08 22:42:55.591478', '7', '2018-02-08 20:42:45-02:00', 1, '[{\"added\": {}}]', 10, 1),
(29, '2018-02-08 22:43:10.570843', '8', '2018-02-08 20:43:01-02:00', 1, '[{\"added\": {}}]', 10, 1),
(30, '2018-02-08 22:43:17.147126', '9', '2018-02-08 20:43:00-02:00', 1, '[{\"added\": {}}]', 10, 1),
(31, '2018-02-08 22:49:33.695629', '1', 'Dispositivo 1', 1, '[{\"added\": {}}]', 9, 1),
(32, '2018-02-08 23:09:50.969423', '2', 'ASSINE O COMBATE: 2', 2, '[{\"changed\": {\"fields\": [\"text\"]}}]', 7, 1),
(33, '2018-02-08 23:10:02.712801', '2', 'ASSINE O COMBATE: 2', 2, '[{\"changed\": {\"fields\": [\"button_color\"]}}]', 7, 1),
(34, '2018-02-08 23:16:16.960352', '2', 'Algar', 1, '[{\"added\": {}}]', 12, 1),
(35, '2018-02-08 23:17:05.953876', '3', 'Blue', 1, '[{\"added\": {}}]', 12, 1),
(36, '2018-02-08 23:17:29.570841', '4', 'Claro', 1, '[{\"added\": {}}]', 12, 1),
(37, '2018-02-08 23:22:48.521895', '5', 'GVT', 1, '[{\"added\": {}}]', 12, 1),
(38, '2018-02-08 23:23:13.084746', '6', 'NET', 1, '[{\"added\": {}}]', 12, 1),
(39, '2018-02-08 23:23:41.495010', '7', 'OI', 1, '[{\"added\": {}}]', 12, 1),
(40, '2018-02-08 23:24:07.841978', '8', 'SKY', 1, '[{\"added\": {}}]', 12, 1),
(41, '2018-02-08 23:24:40.088351', '9', 'VIVO', 1, '[{\"added\": {}}]', 12, 1),
(42, '2018-02-08 23:49:54.062090', '1', 'Globo.com', 1, '[{\"added\": {}}]', 13, 1),
(43, '2018-02-08 23:55:22.172298', '2', 'g1', 1, '[{\"added\": {}}]', 13, 1),
(44, '2018-02-09 00:36:45.969488', '2', 'g1', 2, '[{\"changed\": {\"fields\": [\"order\"]}}]', 13, 1),
(45, '2018-02-09 00:37:09.744289', '1', 'Globo.com', 2, '[]', 13, 1),
(46, '2018-02-09 00:37:17.809019', '1', 'Globo.com', 2, '[]', 13, 1),
(47, '2018-02-09 00:37:35.018682', '1', 'Globo.com', 2, '[{\"changed\": {\"fields\": [\"order\"]}}]', 13, 1),
(48, '2018-02-09 00:37:40.882151', '2', 'g1', 2, '[{\"changed\": {\"fields\": [\"order\"]}}]', 13, 1),
(49, '2018-02-09 00:38:42.965174', '3', 'globoesporte', 1, '[{\"added\": {}}]', 13, 1),
(50, '2018-02-09 00:39:00.092978', '4', 'gshow', 1, '[{\"added\": {}}]', 13, 1),
(51, '2018-02-09 00:39:17.139827', '5', 'famosos & etc', 1, '[{\"added\": {}}]', 13, 1),
(52, '2018-02-09 00:39:18.836030', '5', 'famosos & etc', 2, '[]', 13, 1),
(53, '2018-02-09 00:43:47.919645', '3', '3 - globoesporte', 2, '[{\"changed\": {\"fields\": [\"link\"]}}]', 13, 1),
(54, '2018-02-09 00:43:56.550087', '2', '2 - g1', 2, '[{\"changed\": {\"fields\": [\"link\"]}}]', 13, 1),
(55, '2018-02-09 00:44:08.044542', '4', '4 - gshow', 2, '[{\"changed\": {\"fields\": [\"link\"]}}]', 13, 1),
(56, '2018-02-09 00:44:14.205528', '3', '3 - globoesporte', 2, '[]', 13, 1),
(57, '2018-02-09 00:44:21.048939', '5', '5 - famosos & etc', 2, '[{\"changed\": {\"fields\": [\"link\"]}}]', 13, 1),
(58, '2018-02-09 00:45:02.029910', '6', '6 - vídeos', 1, '[{\"added\": {}}]', 13, 1),
(59, '2018-02-09 01:29:58.841033', '3', 'Configuração Padrão', 2, '[{\"changed\": {\"fields\": [\"welcome_text\"]}}]', 8, 1),
(60, '2018-02-09 01:41:24.572076', '9', '2018-02-08 20:43:00-02:00', 2, '[{\"changed\": {\"fields\": [\"type_weight\", \"fighter1\", \"fighter2\"]}}]', 10, 1),
(61, '2018-02-09 01:43:50.995750', '7', '2018-02-08 20:42:45-02:00', 2, '[{\"changed\": {\"fields\": [\"type_fight\", \"fighter1\", \"fighter2\"]}}]', 10, 1),
(62, '2018-02-09 01:44:01.307974', '6', '2018-02-08 20:42:37-02:00', 2, '[{\"changed\": {\"fields\": [\"type_fight\", \"fighter1\", \"fighter2\"]}}]', 10, 1),
(63, '2018-02-09 01:44:07.871126', '5', '2018-02-08 20:42:19-02:00', 2, '[{\"changed\": {\"fields\": [\"type_fight\", \"fighter1\", \"fighter2\"]}}]', 10, 1),
(64, '2018-02-09 01:44:11.813876', '4', '2018-02-21 20:41:31-03:00', 2, '[{\"changed\": {\"fields\": [\"type_fight\", \"fighter1\", \"fighter2\"]}}]', 10, 1),
(65, '2018-02-09 01:44:32.135095', '9', '2018-02-08 20:43:00-02:00', 2, '[{\"changed\": {\"fields\": [\"type_fight\"]}}]', 10, 1),
(66, '2018-02-09 01:44:52.974084', '8', '2018-02-08 20:43:01-02:00', 2, '[{\"changed\": {\"fields\": [\"type_fight\", \"fighter1\", \"fighter2\"]}}]', 10, 1),
(67, '2018-02-09 01:52:40.620374', '9', '2018-02-08 20:43:00-02:00', 2, '[{\"changed\": {\"fields\": [\"type_fight\", \"fighter1\"]}}]', 10, 1),
(68, '2018-02-09 01:53:19.569947', '8', '2018-02-08 20:43:01-02:00', 2, '[{\"changed\": {\"fields\": [\"fighter1\", \"fighter2\"]}}]', 10, 1),
(69, '2018-02-09 01:53:28.947968', '7', '2018-02-08 20:42:45-02:00', 2, '[{\"changed\": {\"fields\": [\"fighter1\"]}}]', 10, 1),
(70, '2018-02-09 01:53:43.616858', '7', '2018-02-08 20:42:45-02:00', 2, '[{\"changed\": {\"fields\": [\"fighter2\"]}}]', 10, 1),
(71, '2018-02-09 01:53:55.935493', '6', '2018-02-08 20:42:37-02:00', 2, '[{\"changed\": {\"fields\": [\"fighter1\", \"fighter2\"]}}]', 10, 1),
(72, '2018-02-09 01:54:13.284390', '5', '2018-02-08 20:42:19-02:00', 2, '[{\"changed\": {\"fields\": [\"fighter1\", \"fighter2\"]}}]', 10, 1),
(73, '2018-02-09 01:54:35.421520', '4', '2018-02-21 20:41:31-03:00', 2, '[{\"changed\": {\"fields\": [\"fighter1\", \"fighter2\"]}}]', 10, 1),
(74, '2018-02-09 02:39:53.720815', '6', 'NET', 2, '[]', 12, 1),
(75, '2018-02-09 02:42:06.683092', '6', 'NET', 2, '[{\"changed\": {\"fields\": [\"tel1\", \"text_tel1\", \"tel2\", \"text_tel2\"]}}]', 12, 1),
(76, '2018-02-09 02:46:00.123000', '6', 'NET', 2, '[{\"changed\": {\"fields\": [\"extra_buy_mode\"]}}]', 12, 1),
(77, '2018-02-09 03:40:24.346605', '8', 'SKY', 2, '[{\"changed\": {\"fields\": [\"tel1\", \"text_tel1\"]}}]', 12, 1),
(78, '2018-02-09 03:40:34.253122', '5', 'GVT', 2, '[{\"changed\": {\"fields\": [\"tel1\"]}}]', 12, 1),
(79, '2018-02-09 03:43:49.908212', '4', 'Claro', 2, '[{\"changed\": {\"fields\": [\"tel2\", \"text_tel2\"]}}]', 12, 1),
(80, '2018-02-09 03:47:09.116695', '4', 'Claro', 2, '[{\"changed\": {\"fields\": [\"tel1\", \"text_tel1\"]}}]', 12, 1),
(81, '2018-02-09 03:47:25.141269', '4', 'Claro', 2, '[{\"changed\": {\"fields\": [\"text_tel1\"]}}]', 12, 1),
(82, '2018-02-09 03:47:36.741812', '4', 'Claro', 2, '[{\"changed\": {\"fields\": [\"text_tel2\"]}}]', 12, 1),
(83, '2018-02-09 03:47:51.872563', '4', 'Claro', 2, '[{\"changed\": {\"fields\": [\"text_tel1\"]}}]', 12, 1),
(84, '2018-02-09 05:11:07.764736', '2', 'ASSINE O COMBATE: 2', 2, '[]', 7, 1),
(85, '2018-02-09 05:11:16.695109', '2', 'ASSINE O COMBATE: 2', 2, '[{\"changed\": {\"fields\": [\"modified_date\"]}}]', 7, 1),
(86, '2018-02-09 05:11:34.568599', '2', 'ASSINE O COMBATE: 2', 2, '[]', 7, 1),
(87, '2018-02-09 05:11:39.558369', '2', 'ASSINE O COMBATE: 2', 2, '[]', 7, 1),
(88, '2018-02-09 05:12:31.414119', '2', 'ASSINE O COMBATE: 2', 2, '[]', 7, 1),
(89, '2018-02-09 05:13:02.829717', '2', 'ASSINE O COMBATE: 2', 2, '[]', 7, 1),
(90, '2018-02-09 05:14:20.447895', '2', 'ASSINE O COMBATE: 2', 2, '[]', 7, 1),
(91, '2018-02-09 05:16:00.343696', '2', 'ASSINE O COMBATE: 2', 2, '[]', 7, 1),
(92, '2018-02-09 05:16:27.130257', '1', 'ASSINE O COMBATE: 1', 2, '[]', 7, 1),
(93, '2018-02-09 05:22:45.065586', '3', 'Configuração Padrão de 2018-02-09 05:22:45.017555+00:00', 2, '[]', 8, 1),
(94, '2018-02-09 05:30:39.096226', '3', 'Configuração Padrão de 02/09/2018', 2, '[]', 8, 1),
(95, '2018-02-09 05:43:33.136440', '3', 'Configuração Padrão - Última modificação em 02/09/2018', 2, '[{\"changed\": {\"fields\": [\"top_bar\"]}}]', 8, 1),
(96, '2018-02-09 05:43:44.270934', '3', 'Configuração Padrão - Última modificação em 02/09/2018', 2, '[{\"changed\": {\"fields\": [\"top_bar\"]}}]', 8, 1),
(97, '2018-02-09 05:44:04.218143', '2', 'ASSINE O COMBATE: 2', 2, '[{\"changed\": {\"fields\": [\"button_color\"]}}]', 7, 1),
(98, '2018-02-09 05:47:11.460807', '3', 'ASSINE O COMBATE: 3', 1, '[{\"added\": {}}]', 7, 1),
(99, '2018-02-09 05:47:35.711153', '8', 'SKY', 2, '[]', 12, 1),
(100, '2018-02-09 05:48:04.918913', '3', 'ASSINE O COMBATE: 3', 2, '[]', 7, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(5, 'admin', 'logentry'),
(2, 'auth', 'group'),
(1, 'auth', 'permission'),
(3, 'auth', 'user'),
(4, 'contenttypes', 'contenttype'),
(8, 'projeto_combate_app', 'defaultconfig'),
(9, 'projeto_combate_app', 'devicesarea'),
(10, 'projeto_combate_app', 'event'),
(11, 'projeto_combate_app', 'eventdefault'),
(13, 'projeto_combate_app', 'navigationfooter'),
(12, 'projeto_combate_app', 'operator'),
(7, 'projeto_combate_app', 'slide'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Estrutura da tabela `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2018-02-08 17:20:06.380143'),
(2, 'auth', '0001_initial', '2018-02-08 17:20:21.776379'),
(3, 'contenttypes', '0002_remove_content_type_name', '2018-02-08 17:20:23.558374'),
(4, 'auth', '0002_alter_permission_name_max_length', '2018-02-08 17:20:25.059142'),
(5, 'auth', '0003_alter_user_email_max_length', '2018-02-08 17:20:26.356211'),
(6, 'auth', '0004_alter_user_username_opts', '2018-02-08 17:20:26.502817'),
(7, 'auth', '0005_alter_user_last_login_null', '2018-02-08 17:20:27.237854'),
(8, 'auth', '0006_require_contenttypes_0002', '2018-02-08 17:20:27.351435'),
(9, 'auth', '0007_alter_validators_add_error_messages', '2018-02-08 17:20:27.524579'),
(10, 'auth', '0008_alter_user_username_max_length', '2018-02-08 17:20:29.714298'),
(11, 'auth', '0009_alter_user_last_name_max_length', '2018-02-08 17:20:30.927701'),
(12, 'admin', '0001_initial', '2018-02-08 17:20:37.059167'),
(13, 'admin', '0002_logentry_remove_auto_add', '2018-02-08 17:20:37.102695'),
(14, 'projeto_combate_app', '0001_initial', '2018-02-08 17:20:38.264537'),
(15, 'sessions', '0001_initial', '2018-02-08 17:20:38.957606'),
(16, 'projeto_combate_app', '0002_slide_button_text', '2018-02-08 17:22:14.180388'),
(17, 'projeto_combate_app', '0003_auto_20180208_1620', '2018-02-08 18:20:16.612024'),
(18, 'projeto_combate_app', '0004_auto_20180208_1943', '2018-02-08 21:43:39.649795'),
(19, 'projeto_combate_app', '0005_defaultconfigs_published_date', '2018-02-08 21:48:11.462918'),
(20, 'projeto_combate_app', '0006_auto_20180208_1949', '2018-02-08 21:49:32.645138'),
(21, 'projeto_combate_app', '0007_auto_20180208_2005', '2018-02-08 22:05:43.624653'),
(22, 'projeto_combate_app', '0008_auto_20180208_2007', '2018-02-08 22:07:44.036576'),
(23, 'projeto_combate_app', '0009_auto_20180208_2008', '2018-02-08 22:08:50.789152'),
(24, 'projeto_combate_app', '0010_defaultconfigs_title', '2018-02-08 22:13:14.147646'),
(25, 'projeto_combate_app', '0011_defaultconfigs_copyright_text', '2018-02-08 22:17:14.262251'),
(26, 'projeto_combate_app', '0012_auto_20180208_2021', '2018-02-08 22:21:58.031008'),
(27, 'projeto_combate_app', '0013_auto_20180208_2022', '2018-02-08 22:22:56.896809'),
(28, 'projeto_combate_app', '0014_auto_20180208_2025', '2018-02-08 22:25:08.895931'),
(29, 'projeto_combate_app', '0015_auto_20180208_2026', '2018-02-08 22:26:12.569812'),
(30, 'projeto_combate_app', '0016_auto_20180208_2041', '2018-02-08 22:41:28.882917'),
(31, 'projeto_combate_app', '0017_auto_20180208_2047', '2018-02-08 22:47:43.480911'),
(32, 'projeto_combate_app', '0018_auto_20180208_2112', '2018-02-08 23:13:00.205274'),
(33, 'projeto_combate_app', '0019_auto_20180208_2115', '2018-02-08 23:15:44.822899'),
(34, 'projeto_combate_app', '0020_navigationfooter', '2018-02-08 23:47:48.243111'),
(35, 'projeto_combate_app', '0021_auto_20180208_2148', '2018-02-08 23:48:29.784330'),
(36, 'projeto_combate_app', '0022_navigationfooter_order', '2018-02-09 00:27:07.925166'),
(37, 'projeto_combate_app', '0023_auto_20180208_2236', '2018-02-09 00:36:26.018030'),
(38, 'projeto_combate_app', '0024_auto_20180208_2237', '2018-02-09 00:37:06.502042'),
(39, 'projeto_combate_app', '0025_auto_20180208_2237', '2018-02-09 00:37:31.885286'),
(40, 'projeto_combate_app', '0026_auto_20180208_2252', '2018-02-09 00:52:14.770422'),
(41, 'projeto_combate_app', '0027_auto_20180208_2256', '2018-02-09 00:56:18.794698'),
(42, 'projeto_combate_app', '0028_auto_20180208_2256', '2018-02-09 00:57:05.411056'),
(43, 'projeto_combate_app', '0029_auto_20180208_2303', '2018-02-09 01:03:48.111280'),
(44, 'projeto_combate_app', '0030_auto_20180208_2308', '2018-02-09 01:08:33.017678'),
(45, 'projeto_combate_app', '0031_auto_20180208_2309', '2018-02-09 01:09:57.061882'),
(46, 'projeto_combate_app', '0032_auto_20180208_2311', '2018-02-09 01:11:27.446844'),
(47, 'projeto_combate_app', '0033_auto_20180208_2340', '2018-02-09 01:40:41.486602'),
(48, 'projeto_combate_app', '0034_auto_20180208_2342', '2018-02-09 01:42:13.253204'),
(49, 'projeto_combate_app', '0035_auto_20180209_0038', '2018-02-09 02:38:25.740357'),
(50, 'projeto_combate_app', '0036_auto_20180209_0039', '2018-02-09 02:39:35.991418'),
(51, 'projeto_combate_app', '0037_auto_20180209_0041', '2018-02-09 02:41:39.759496'),
(52, 'projeto_combate_app', '0038_operator_extra_buy_mode', '2018-02-09 02:43:31.659182'),
(53, 'projeto_combate_app', '0039_auto_20180209_0044', '2018-02-09 02:44:44.645305'),
(54, 'projeto_combate_app', '0040_auto_20180209_0115', '2018-02-09 03:15:08.562841'),
(55, 'projeto_combate_app', '0041_auto_20180209_0314', '2018-02-09 05:14:12.837147'),
(56, 'projeto_combate_app', '0042_auto_20180209_0315', '2018-02-09 05:15:53.395360'),
(57, 'projeto_combate_app', '0041_auto_20180209_0342', '2018-02-09 05:42:49.026501'),
(58, 'projeto_combate_app', '0042_auto_20180209_0344', '2018-02-09 05:44:40.355491');

-- --------------------------------------------------------

--
-- Estrutura da tabela `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('7w89h18y46w4z071hodkg0d97n34zsjr', 'OTJkNDZjNjYyNTY4ZDM2ZTBmNTdlYTM2NmUwMjUzNzg1YjU2YzYzZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3MGRjZmM3MWQ1ZTY4MGU1MjZjOTBmMjAwNDhhMjMzODFjOGFjNDM2In0=', '2018-02-22 17:23:27.784731');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto_combate_app_defaultconfig`
--

CREATE TABLE `projeto_combate_app_defaultconfig` (
  `id` int(11) NOT NULL,
  `top_bar` varchar(18) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `welcome_title` varchar(200) NOT NULL,
  `welcome_text` longtext NOT NULL,
  `author_id` int(11) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `modified_date` datetime(6) NOT NULL,
  `title` varchar(100) NOT NULL,
  `copyright_text` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `projeto_combate_app_defaultconfig`
--

INSERT INTO `projeto_combate_app_defaultconfig` (`id`, `top_bar`, `logo`, `welcome_title`, `welcome_text`, `author_id`, `created_date`, `modified_date`, `title`, `copyright_text`) VALUES
(3, '#C10E05', 'ico-combate-light-min_Qnyd9z5.png', 'A MAIOR COBERTURA DO UFC', '<p>O Combate exibe <strong>ao vivo e na íntegra todas as edições do UFC</strong> com exclusividade, além de uma seleção com mais de 1.000 grandes desafios de MMA. E o que é melhor: quando e onde você quiser, sem custo adicional.</p>', 1, '2018-02-08 21:49:31.000000', '2018-02-09 05:43:44.143973', 'Configuração Padrão', '© Copyright 2000-2018 Globo Comunicação e Participações S.A.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto_combate_app_devicesarea`
--

CREATE TABLE `projeto_combate_app_devicesarea` (
  `id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `text` longtext NOT NULL,
  `button_link` varchar(200) NOT NULL,
  `author_id` int(11) NOT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  `modified_date` datetime(6) DEFAULT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `projeto_combate_app_devicesarea`
--

INSERT INTO `projeto_combate_app_devicesarea` (`id`, `image`, `text`, `button_link`, `author_id`, `created_date`, `modified_date`, `title`) VALUES
(1, 'combate-devices-min.png', 'ASSISTA AO VIVO E REVEJA AS LUTAS QUANDO QUISER. EXCLUSIVO PARA ASSINANTES.', 'https://globosatplay.globo.com/combate/', 1, '2018-02-08 22:48:44.000000', '2018-02-08 22:49:16.000000', 'Dispositivo 1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto_combate_app_event`
--

CREATE TABLE `projeto_combate_app_event` (
  `id` int(11) NOT NULL,
  `date` datetime(6) NOT NULL,
  `image` varchar(100) NOT NULL,
  `modified_date` datetime(6) DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  `fighter1` varchar(200) NOT NULL,
  `fighter2` varchar(200) NOT NULL,
  `type_fight` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `projeto_combate_app_event`
--

INSERT INTO `projeto_combate_app_event` (`id`, `date`, `image`, `modified_date`, `author_id`, `created_date`, `fighter1`, `fighter2`, `type_fight`) VALUES
(4, '2018-02-21 23:41:31.000000', 'molde-min_BsfDTap.png', '2018-02-08 22:41:40.000000', 1, '2018-02-08 22:41:31.000000', 'Mathew Henry', 'Jake Sttwart', 'Luta Principal'),
(5, '2018-02-08 22:42:19.000000', 'molde-min_TTMiBcD.png', '2018-02-08 22:42:26.000000', 1, '2018-02-08 22:42:19.000000', 'Jason San', 'Jan Willians', 'Cinturão Peso Galo'),
(6, '2018-02-08 22:42:37.000000', 'molde-min_nUSh80K.png', '2018-02-08 22:42:41.000000', 1, '2018-02-08 22:42:37.000000', 'James Elton', 'Elisberto', 'Cinturão Peso Médio'),
(7, '2018-02-08 22:42:45.000000', 'molde-min_AAIl7zo.png', '2018-02-08 22:42:54.000000', 1, '2018-02-08 22:42:45.000000', 'G. Rocha', 'Vinícius B.', 'Luta Principal'),
(8, '2018-02-08 22:43:01.000000', 'molde-min_9voFLVP.png', '2018-02-08 22:43:09.000000', 1, '2018-02-08 22:43:01.000000', 'Minotauro', 'Figueiredo', 'Cinturão Peso Pesado'),
(9, '2018-02-08 22:43:00.000000', 'molde-min_1WYWXAA.png', '2018-02-08 22:43:14.000000', 1, '2018-02-08 22:43:00.000000', 'Sonel', 'Minotauro', 'Machida');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto_combate_app_eventdefault`
--

CREATE TABLE `projeto_combate_app_eventdefault` (
  `id` int(11) NOT NULL,
  `event_title` varchar(200) NOT NULL,
  `events_button_text` varchar(20) NOT NULL,
  `events_button_color` varchar(7) NOT NULL,
  `author_id` int(11) NOT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  `modified_date` datetime(6) DEFAULT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `projeto_combate_app_eventdefault`
--

INSERT INTO `projeto_combate_app_eventdefault` (`id`, `event_title`, `events_button_text`, `events_button_color`, `author_id`, `created_date`, `modified_date`, `title`) VALUES
(2, 'PRÓXIMOS EVENTOS EXCLUSIVOS', 'Assine Já', '#c10e05', 1, '2018-02-08 22:25:06.000000', '2018-02-08 22:26:22.000000', 'Configurações Básicas do Evento');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto_combate_app_navigationfooter`
--

CREATE TABLE `projeto_combate_app_navigationfooter` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `order` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `projeto_combate_app_navigationfooter`
--

INSERT INTO `projeto_combate_app_navigationfooter` (`id`, `name`, `link`, `image`, `author_id`, `order`) VALUES
(1, 'Globo.com', 'https://globo.com.br', 'logo_footer.png', 1, 1),
(2, 'g1', 'http://g1.globo.com', '', 1, 2),
(3, 'globoesporte', 'http://globoesporte.globo.com', '', 1, 3),
(4, 'gshow', 'https://gshow.globo.com', '', 1, 4),
(5, 'famosos & etc', 'http://famosos.globo.com', '', 1, 5),
(6, 'vídeos', 'https://globoplay.globo.com', '', 1, 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto_combate_app_operator`
--

CREATE TABLE `projeto_combate_app_operator` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `site` varchar(200) DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `tel1` varchar(200),
  `text_tel1` varchar(200) DEFAULT NULL,
  `text_tel2` varchar(200) DEFAULT NULL,
  `tel2` varchar(200) DEFAULT NULL,
  `extra_buy_mode` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `projeto_combate_app_operator`
--

INSERT INTO `projeto_combate_app_operator` (`id`, `name`, `logo`, `site`, `author_id`, `tel1`, `text_tel1`, `text_tel2`, `tel2`, `extra_buy_mode`) VALUES
(2, 'Algar', 'logo-algar-min_sfNtZMz.png', NULL, 1, '', '', '', NULL, ''),
(3, 'Blue', 'logo-blue-min.png', NULL, 1, '', '', '', NULL, ''),
(4, 'Claro', 'logo-claro-min.png', 'http://clarotv.claro.com.br/combate/', 1, '(00) 0000-0000', 'None', 'None', '(00) 0000-0000', ''),
(5, 'GVT', 'logo-gvt-min.png', NULL, 1, '(00) 0000-0000', NULL, NULL, NULL, ''),
(6, 'NET', 'logo-net-min.png', 'http://www.netcombo.com.br/tv-por-assinatura/pay-per-view/combate', 1, '4004-8844', 'Capitais', 'Demais Localidades', '0800-726-0800', '<p>Acesse o <strong>NET NOW</strong>, vá para o <strong>NOW Clube</strong>, entre no <strong>Combate</strong> e escolha a opção <strong>Assinar Pacote</strong>. Agora, basta concluir as etapas até finalizar sua assinatura!</p>'),
(7, 'OI', 'logo-oi-min.png', NULL, 1, '', '', '', NULL, ''),
(8, 'SKY', 'logo-sky-min.png', 'https://www.sky.com.br/site/compra-online/combate/', 1, '(00) 0000-0000', 'Texto para Sky', NULL, NULL, ''),
(9, 'VIVO', 'logo-vivo-min.png', 'https://assine.vivo.com.br/tv-por-assinatura/conteudos-opcionais', 1, '', '', '', NULL, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto_combate_app_slide`
--

CREATE TABLE `projeto_combate_app_slide` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `subtitle` varchar(150) NOT NULL,
  `image` varchar(100) NOT NULL,
  `text` longtext NOT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  `button_color` varchar(18) NOT NULL,
  `author_id` int(11) NOT NULL,
  `button_text` varchar(20) NOT NULL,
  `modified_date` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `projeto_combate_app_slide`
--

INSERT INTO `projeto_combate_app_slide` (`id`, `title`, `subtitle`, `image`, `text`, `created_date`, `button_color`, `author_id`, `button_text`, `modified_date`) VALUES
(1, 'ASSINE O COMBATE: 1', 'LUTAS AO VIVO', 'slide1-min_DuL9ARn.jpg', 'VEJA TODAS AS LUTAS DO UFC E O MELHOR DO MMA', '2018-02-08 17:23:32.000000', '#d32127', 1, 'ASSINE JÁ', '2018-02-09 05:16:27.088229'),
(2, 'ASSINE O COMBATE: 2', 'LUTAS AO VIVO', 'slide2-min_Pw1Y4QK.jpg', 'VEJA TODAS AS LUTAS DO UFC E O MELHOR DO MMA', '2017-04-03 17:25:27.000000', '#D32127', 1, 'ASSINE HOJE', '2018-02-09 05:44:04.202132'),
(3, 'ASSINE O COMBATE: 3', 'LUTAS AO VIVO', 'slide3.jpg', 'VEJA TODAS AS LUTAS DO UFC E O MELHOR DO MMA', '2019-04-19 05:47:11.459807', '#D32127', 1, 'ASSINE JÁ', '2018-02-09 05:48:04.900240');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `projeto_combate_app_defaultconfig`
--
ALTER TABLE `projeto_combate_app_defaultconfig`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projeto_combate_app__author_id_eae3d60a_fk_auth_user` (`author_id`);

--
-- Indexes for table `projeto_combate_app_devicesarea`
--
ALTER TABLE `projeto_combate_app_devicesarea`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projeto_combate_app__author_id_13fa87ad_fk_auth_user` (`author_id`);

--
-- Indexes for table `projeto_combate_app_event`
--
ALTER TABLE `projeto_combate_app_event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projeto_combate_app_event_author_id_728a4ee5_fk_auth_user_id` (`author_id`);

--
-- Indexes for table `projeto_combate_app_eventdefault`
--
ALTER TABLE `projeto_combate_app_eventdefault`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projeto_combate_app__author_id_a90924e1_fk_auth_user` (`author_id`);

--
-- Indexes for table `projeto_combate_app_navigationfooter`
--
ALTER TABLE `projeto_combate_app_navigationfooter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projeto_combate_app__author_id_f9fef5c9_fk_auth_user` (`author_id`);

--
-- Indexes for table `projeto_combate_app_operator`
--
ALTER TABLE `projeto_combate_app_operator`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projeto_combate_app_operator_author_id_256f703c_fk_auth_user_id` (`author_id`);

--
-- Indexes for table `projeto_combate_app_slide`
--
ALTER TABLE `projeto_combate_app_slide`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projeto_combate_app_slide_author_id_7c9d44c2_fk_auth_user_id` (`author_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `projeto_combate_app_defaultconfig`
--
ALTER TABLE `projeto_combate_app_defaultconfig`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `projeto_combate_app_devicesarea`
--
ALTER TABLE `projeto_combate_app_devicesarea`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `projeto_combate_app_event`
--
ALTER TABLE `projeto_combate_app_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `projeto_combate_app_eventdefault`
--
ALTER TABLE `projeto_combate_app_eventdefault`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `projeto_combate_app_navigationfooter`
--
ALTER TABLE `projeto_combate_app_navigationfooter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `projeto_combate_app_operator`
--
ALTER TABLE `projeto_combate_app_operator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `projeto_combate_app_slide`
--
ALTER TABLE `projeto_combate_app_slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Limitadores para a tabela `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Limitadores para a tabela `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Limitadores para a tabela `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Limitadores para a tabela `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Limitadores para a tabela `projeto_combate_app_defaultconfig`
--
ALTER TABLE `projeto_combate_app_defaultconfig`
  ADD CONSTRAINT `projeto_combate_app__author_id_eae3d60a_fk_auth_user` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`);

--
-- Limitadores para a tabela `projeto_combate_app_devicesarea`
--
ALTER TABLE `projeto_combate_app_devicesarea`
  ADD CONSTRAINT `projeto_combate_app__author_id_13fa87ad_fk_auth_user` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`);

--
-- Limitadores para a tabela `projeto_combate_app_event`
--
ALTER TABLE `projeto_combate_app_event`
  ADD CONSTRAINT `projeto_combate_app_event_author_id_728a4ee5_fk_auth_user_id` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`);

--
-- Limitadores para a tabela `projeto_combate_app_eventdefault`
--
ALTER TABLE `projeto_combate_app_eventdefault`
  ADD CONSTRAINT `projeto_combate_app__author_id_a90924e1_fk_auth_user` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`);

--
-- Limitadores para a tabela `projeto_combate_app_navigationfooter`
--
ALTER TABLE `projeto_combate_app_navigationfooter`
  ADD CONSTRAINT `projeto_combate_app__author_id_f9fef5c9_fk_auth_user` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`);

--
-- Limitadores para a tabela `projeto_combate_app_operator`
--
ALTER TABLE `projeto_combate_app_operator`
  ADD CONSTRAINT `projeto_combate_app_operator_author_id_256f703c_fk_auth_user_id` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`);

--
-- Limitadores para a tabela `projeto_combate_app_slide`
--
ALTER TABLE `projeto_combate_app_slide`
  ADD CONSTRAINT `projeto_combate_app_slide_author_id_7c9d44c2_fk_auth_user_id` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
